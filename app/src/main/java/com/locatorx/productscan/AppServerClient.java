package com.locatorx.productscan;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AppServerClient
{
    private static final boolean STAGING = BuildConfig.STAGING;

    public static String getCSMToken()
    {
        if (STAGING) {
            return "62418f50-44e9-4173-aaeb-0521f8d64491";
        }
        return "a90c7ec1-f45e-4807-9539-604096adf41d";
    }

    public static String getUrlBase()
    {
        if (STAGING) {
            return "https://api-staging.locatorx.com/lx-atlas/api/";
        }
        return "https://api.locatorx.com/lx-atlas/api/";
    }

    public static String getScanPageUrl()
    {
        if (STAGING) {
            return "https://lxproductscan-staging.locatorx.com/home";
        }
        return "https://lxproductscan.locatorx.com/home";
    }

    public static String getHistoryPageUrl()
    {
        if (STAGING) {
            return "https://lxproductscan-staging.locatorx.com/history";
        }
        return "https://lxproductscan.locatorx.com/history";
    }

    public static String getSettingsPageUrl()
    {
        if (STAGING) {
            return "https://lxproductscan-staging.locatorx.com/settings";
        }
        return "https://lxproductscan.locatorx.com/settings";
    }

    public static Map<String,Object> fetchNFCString(String url)
    {
        Map<String,Object> result = new HashMap<String,Object>();

        try {
            return JSONUtil.fetchJson(url, null, null);

        } catch (Exception e) {
        }

        return result;
    }

    public static Map<String,Object> processAppSettings()
    {
        Map<String,Object> result = new HashMap<String,Object>();

        try {
            String url = getUrlBase() + "appSettings/mobile/LXConnect";

            Map<String,String> headerMap = new HashMap<String,String>();
            headerMap.put("CSM", getCSMToken());

            return JSONUtil.fetchJson(url, null, headerMap);

        } catch (Exception e) {
        }

        return result;
    }

    public static Map<String,Object> processScanFromApp(String assetId, String qrHash, Double latitude, Double longitude)
    {
        Map<String,Object> result = new HashMap<String,Object>();

        try {
            String url = null;
            Map<String,Object> postBody = new HashMap<String,Object>();
            if (assetId != null) {
                url = getUrlBase() + "assets/csm/mobile/"+assetId+"/scanFromApp";
            }

            postBody.put("latitude", latitude);
            postBody.put("longitude", longitude);
            if (qrHash != null)
                postBody.put("qrHash", qrHash);

            Map<String,String> headerMap = new HashMap<String,String>();
            headerMap.put("CSM", getCSMToken());

            return JSONUtil.fetchJson(url, postBody, headerMap);

        } catch (Exception e) {
        }

        return result;
    }

}
