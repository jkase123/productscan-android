package com.locatorx.productscan;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {
    private static final int BARCODE_READER_ACTIVITY_REQUEST = 1208;

//    public static final String ERROR_DETECTED = "No NFC tag detected!";
//    public static final String WRITE_SUCCESS = "Text written to the NFC tag successfully!";
//    public static final String WRITE_ERROR = "Error during writing, is the NFC tag close enough to your device?";

//    public static final int kScanModeBoth = 0;
    public static final int kScanModeQR = 1;
//    public static final int kScanModeNFC = 2;

    private static final String kHomePageUrl = "https://connectx-staging.locatorx.com/home";

//    NfcAdapter nfcAdapter;
//    PendingIntent pendingIntent;
//    IntentFilter tagFilters[];
    boolean writeMode;

    Tag myTag;
    WebView mWebView, mScanWebView, mHistoryWebView, mSettingsWebView;
    ImageView mButtonBackDis, mButtonBack;
    RelativeLayout mSecurityBackgroundGreen, mSecurityBackgroundRed;
    Vibrator myVib;
    MediaPlayer mp;
    public BottomNavigationView tabs;

    private FusedLocationProviderClient fusedLocationClient;

    int iScanMode = kScanModeQR;
    String url;
    boolean hasQR = false, hasNFC = false;
    private String barcodeUrl, assetUUID, qrHash;
//    private String barcodeUrl, assetUUID, nfcText;

    private final String BUNDLE_NFC_TEXT =  "NFC_TEXT";
    private final String BUNDLE_URL =       "URL";
    private final String BUNDLE_HAS_NFC =   "HAS_NFC";
    private final String BUNDLE_HAS_QR =    "HAS_QR";
    private final String BUNDLE_UUID =      "UUID";
    private final String BUNDLE_RAW_URL =   "RAW_URL";
    private final String BUNDLE_SCAN_MODE = "SCAN_MODE";

    private void configureWebView(WebView webView)
    {
        webView.clearCache(true);
        webView.clearHistory();

        WebSettings webSettings = webView.getSettings();
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptEnabled(true);
//        webSettings.setTextZoom(getTextZoomPercent());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Support http and https content
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsPrompt(WebView view, String url1, String message,
                                      String defaultValue, JsPromptResult jsResult) {
                String result = handlePromptMessage(message);
                jsResult.confirm(result);
                return true;
            }
        });
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                mProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.i("pageFinished", "***** can go back "+(view.canGoBack() ? "true" : "false"));

                enableBackButton(view.canGoBack());
//                mProgress.setVisibility(View.GONE);
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // configure WebView
        //
        mWebView = mScanWebView = findViewById(R.id.scanWebview);
        mHistoryWebView = findViewById(R.id.historyWebview);
        mSettingsWebView = findViewById(R.id.settingsWebview);

        mButtonBackDis = findViewById(R.id.btn_back_dis);
        mButtonBack = findViewById(R.id.btn_back);
        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("backButton", "***** go back");
                mWebView.goBack();
            }
        });

        mSecurityBackgroundGreen = findViewById(R.id.securityBackgroundGreen);
        mSecurityBackgroundRed = findViewById(R.id.securityBackgroundRed);

        configureWebView(mScanWebView);
        configureWebView(mHistoryWebView);
        configureWebView(mSettingsWebView);

        mScanWebView.loadUrl(AppServerClient.getScanPageUrl());
        mHistoryWebView.loadUrl(AppServerClient.getHistoryPageUrl());
        mSettingsWebView.loadUrl(AppServerClient.getSettingsPageUrl());

        tabs = findViewById(R.id.tabs);
        tabs.setOnNavigationItemSelectedListener(this);
        tabs.inflateMenu(R.menu.bottom_tab);
        tabs.setSelectedItemId(R.id.tab_scan);

        enableBackButton(true);

        /*
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
        }
        readFromIntent(getIntent());

        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        try {
            tagDetected.addDataType("* / *");        // remove spaces to re-enable
        } catch (IntentFilter.MalformedMimeTypeException e) {
        }
        tagFilters = new IntentFilter[]{tagDetected};
*/
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();

        locationCallback = new LocationCallback() {
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    currentLocation = location;
                    Log.i("MainActivity*****", "Location 1: "+location.getLatitude()+", "+location.getLongitude());
                }
            };
        };
        myVib = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        mp = MediaPlayer.create(this, R.raw.chime);

        if (getIntent().getData() != null) {
            String incomingUrl = getIntent().getData().toString();
            Log.i("INTENT",incomingUrl);
        }
    }

    private void enableBackButton(boolean enable)
    {
        if (enable) {
            mButtonBack.setVisibility(View.VISIBLE);
            mButtonBackDis.setVisibility(View.GONE);
        } else {
            mButtonBack.setVisibility(View.GONE);
            mButtonBackDis.setVisibility(View.VISIBLE);
        }
    }

    private String handlePromptMessage(String prompt)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String result = "done";

        if (prompt.equals("scan")) {
            launchBarCodeActivity();
 //       } else if (prompt.equals("logout")) {
 //           handleLogout(sharedPreferences);
        } else if (prompt.equals("settings")) {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.fromParts("package", "com.locatorx.productscan", null));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (prompt.equals("scrollEnable")) {
        } else if (prompt.equals("scrollDisable")) {
        } else if (prompt.equals("enableBack")) {
            enableBackButton(true);
        } else if (prompt.equals("disableBack")) {
            enableBackButton(false);
        } else if (prompt.equals("loadAssetHistory")) {
            List<Map<String,Object>> eventsList = getSortedEventList();
            return JSONUtil.jsonToString(eventsList);

        } else if (prompt.equals("saveAssetHistory=")) {

        } else if (prompt.equals("openUrl")) {
        } else if (prompt.startsWith("get=")) {
            String parts[] = prompt.split("=");
            if (parts.length == 2)
                result = sharedPreferences.getString(parts[1], "false");
        } else if (prompt.startsWith("set=")) {
            String parts[] = prompt.split("=");
            if (parts.length == 3) {
                String key = parts[1];
                result = parts[2];
                sharedPreferences.edit().putString(key, result).commit();
            }
        } else if (prompt.startsWith("toggle=")) {
            String parts[] = prompt.split("=");
            if (parts.length == 2) {
                String key = parts[1];
                result = sharedPreferences.getString(key, "false");
                if (result.equals("false"))
                    result = "true";
                else
                    result = "false";
                sharedPreferences.edit().putString(key, result).commit();
            }
        }
        return result;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tab_scan: {
                mWebView = mScanWebView;
                mScanWebView.setVisibility(View.VISIBLE);
                mHistoryWebView.setVisibility(View.GONE);
                mSettingsWebView.setVisibility(View.GONE);
                break;
            }
            case R.id.tab_history: {
                mWebView = mHistoryWebView;
                mScanWebView.setVisibility(View.GONE);
                mHistoryWebView.setVisibility(View.VISIBLE);
                mSettingsWebView.setVisibility(View.GONE);
                mHistoryWebView.loadUrl(AppServerClient.getHistoryPageUrl());
                break;
            }
            case R.id.tab_settings: {
                mWebView = mSettingsWebView;
                mScanWebView.setVisibility(View.GONE);
                mHistoryWebView.setVisibility(View.GONE);
                mSettingsWebView.setVisibility(View.VISIBLE);
                break;
            }
            default:

        }
        enableBackButton(mWebView.canGoBack());
        return true;
    }

    /*
    @Override
    protected void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }
*/
    @Override
    public void onResume()
    {
        super.onResume();
        startLocationupdates();
//        Log.i("MainActivity*****", "onResume: hasQR="+ hasQR +" hasNFC="+hasNFC);
//        nfcAdapter.enableForegroundDispatch(this, pendingIntent, tagFilters, null);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        /*
        readFromIntent(intent);
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())){
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
        */
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        Log.i("MainActivity*****", "onSave");

//        if (nfcText != null)
//            outState.putString(BUNDLE_NFC_TEXT, nfcText);
        if (url != null)
            outState.putString(BUNDLE_URL, url);
        if (assetUUID != null)
            outState.putString(BUNDLE_UUID, assetUUID);
        if (barcodeUrl != null)
            outState.putString(BUNDLE_RAW_URL, barcodeUrl);

        outState.putInt(BUNDLE_HAS_NFC, (hasNFC ? 1 : 0));
        outState.putInt(BUNDLE_HAS_QR, (hasQR ? 1 : 0));
        outState.putInt(BUNDLE_SCAN_MODE, iScanMode);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        Log.i("MainActivity*****", "onRestore");

        super.onRestoreInstanceState(savedInstanceState);

//        nfcText = savedInstanceState.getString(BUNDLE_NFC_TEXT);
        url = savedInstanceState.getString(BUNDLE_URL);
        assetUUID = savedInstanceState.getString(BUNDLE_UUID);
        barcodeUrl = savedInstanceState.getString(BUNDLE_RAW_URL);
        iScanMode = savedInstanceState.getInt(BUNDLE_SCAN_MODE);
        hasNFC = savedInstanceState.getInt(BUNDLE_HAS_NFC) == 1;
        hasQR = savedInstanceState.getInt(BUNDLE_HAS_QR) == 1;
    }

    private Location currentLocation = null;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private final int kPermissionsFineLocation = 1000;

    private void startLocationupdates()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PERMISSION_GRANTED) {
            Log.i("MainActivity", "Location Permission Not Granted");
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        kPermissionsFineLocation);
            }
        } else {

            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                currentLocation = location;
                                Log.i("MainActivity", "Location 2: "+location.getLatitude()+", "+location.getLongitude());
                            }
                        }
                    });

            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case kPermissionsFineLocation: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationupdates();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    /*
    private void readFromIntent(Intent intent)
    {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMsgs != null)
                Log.i("MainActivity*****", "found tag with "+rawMsgs.length+" rawMsgs");
            else
                Log.i("MainActivity*****", "found tag with no rawMsgs");

            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            buildTagViews(msgs);
        }
    }

    private void buildTagViews(NdefMessage[] msgs)
    {
        if (msgs == null || msgs.length == 0) return;

        String text = "";
//        String tagId = new String(msgs[0].getRecords()[0].getType());
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16"; // Get the Text Encoding
        int languageCodeLength = payload[0] & 0063; // Get the Language Code, e.g. "en"
        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");

        try {
            hasNFC = true;
            nfcText = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
            Log.i("MainActivity*****","NFC: "+nfcText);
            if (iScanMode == kScanModeNFC || (iScanMode == kScanModeBoth && hasQR)) {
                if (!hasQR) {
                    String parts[] = nfcText.split("\\|");
                    if (parts != null && parts.length == 4) {
                        assetUUID = parts[2];
                    }
                }
                fetchScanResults();
            } else if (iScanMode == kScanModeBoth) {
                launchBarCodeActivity();
//                Toast.makeText(this,"Please press the Scan button to complete the Scan", Toast.LENGTH_SHORT).show();
            }

        } catch (UnsupportedEncodingException e) {
            Log.e("UnsupportedEncoding", e.toString());
        }
    }
*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
                /*
            case R.id.btn_scan:
                launchBarCodeActivity();
                break;
            case R.id.btn_settings: {
                DialogFragment dialog = SettingsFragment.newInstance(iAffinity, iScanMode);
                dialog.show(getSupportFragmentManager(), "SettingsFragment");
            }
                break;
                */
        }
    }

    private void launchBarCodeActivity()
    {
        Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(this, true, false);
        startActivityForResult(launchIntent, BARCODE_READER_ACTIVITY_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(this, "error in  scanning", Toast.LENGTH_SHORT).show();
            return;
        }

        if (requestCode == BARCODE_READER_ACTIVITY_REQUEST && data != null) {
            Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);
//            Toast.makeText(this, barcode.rawValue, Toast.LENGTH_SHORT).show();
            barcodeUrl = barcode.rawValue;
            assetUUID = qrHash = null;
            parseQRScan();
        }

    }

    private void parseQRScan()
    {
        try {
            URL url = new URL(barcodeUrl);
            String params[] = url.getQuery().split("&");
            for (String param : params) {
                String tagComponents[] = param.split("=");
                switch (tagComponents[0]) {
                    case "assetId":
                        assetUUID = tagComponents[1];
                        break;
                    case "hash":
                        qrHash = tagComponents[1];
                        break;
                }
            }
            if (assetUUID == null) {
                String parts[] = url.getPath().split("/");
                for (String part : parts) {
                    if (part.length() == 36) {
                        try {
                            UUID uuid = UUID.fromString(part);  // make sure it's a UUID
                            assetUUID = part;
                            break;
                        } catch (Exception e) {

                        }
                    }
                }
            }

            hasQR = true;

        } catch (Exception e) {

        }

        fetchScanResults();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPreferences.getString("sound", "false").equals("true"))
            mp.start();

        if (sharedPreferences.getString("vibrate", "false").equals("true"))
            myVib.vibrate(50);

   }

    private void fetchScanResults()
    {
        if (currentLocation == null) {
            Toast.makeText(this,"No Location information Available", Toast.LENGTH_SHORT).show();
            return;
        }
        new ScanTask().execute(new ScanParams(assetUUID, qrHash, currentLocation.getLatitude(), currentLocation.getLongitude()));
    }

    private void processScanResults(Map<String, Object> map)
    {
        hasQR = hasNFC = false;
        assetUUID = barcodeUrl = null;
//        assetUUID = barcodeUrl = nfcText = null;

        Map<String, Object> json = JSONUtil.getSafeMap(map, "json");
        Log.i("MainActivity*****", "json: " + json);
        if (JSONUtil.getSafeBooleanAlt(json, "success")) {
            showSecurityElements(JSONUtil.getSafeBooleanAlt(json, "isCounterfeit"),
                    JSONUtil.getSafeBooleanAlt(json, "missingQR"),
                    JSONUtil.getSafeBooleanAlt(json, "missingNFC"),
                    JSONUtil.getSafeBooleanAlt(json, "nfcValid"));
        } else {
            Toast.makeText(this,"Invalid QR Code", Toast.LENGTH_SHORT).show();
        }
        Map<String,Object> assetMap = JSONUtil.getSafeMap(json, "asset");
        if (assetUUID == null && assetMap != null) {
            assetUUID = JSONUtil.getSafeString(assetMap, "assetId");
        }
        /*
        if (iScanMode == kScanModeNFC) {
            if (json.containsKey("asset")) {
                Map<String, Object> assetJson = JSONUtil.getSafeMap(json, "asset");
                url = "https://connectx-staging.locatorx.com/" + affinity + "/" + JSONUtil.getSafeString(assetJson, "assetId");
            }
        }
        */
        if (assetUUID != null) {
            url = JSONUtil.getSafeString(json, "url");
            Log.i("processScanResults", "url: "+url);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            if (sharedPreferences.getString("history", "false").equals("true"))
                addEvent(assetMap);

            mScanWebView.loadUrl(url);
        }
    }

    private List<Map<String,Object>> getSortedEventList()
    {
        List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();

        try {
            Map<String, Object> eventsJson = JSONUtil.loadJSONData(this, "events");
            TreeMap<Integer, Map<String, Object>> sortedMap = new TreeMap<Integer, Map<String, Object>>();
            for (String key : eventsJson.keySet()) {
                Map<String, Object> eventMap = JSONUtil.getSafeMap(eventsJson, key);
                sortedMap.put(JSONUtil.getSafeInteger(eventMap, "timeOfVisit"), eventMap);
            }
            for (Integer key : sortedMap.keySet()) {
                result.add(0, sortedMap.get(key));
            }
        } catch (Exception e) {

        }
        return result;
    }

    private void addEvent(Map<String,Object> assetMapIn)
    {
        Map<String,Object> eventsJson = JSONUtil.loadJSONData(this, "events");
//        Map<String,Object> eventsJson = new HashMap<String,Object>();
        Map<String,Object> assetMap = new HashMap<String,Object>();
        Date timeOfVisit = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        assetMap.put("assetId", assetUUID);
        assetMap.put("url", url);
        assetMap.put("tag", JSONUtil.getSafeString(assetMapIn, "tag"));
        assetMap.put("timeOfVisit", (timeOfVisit.getTime() + 500) / 1000);
        assetMap.put("timeOfVisitString", sdf.format(timeOfVisit));
        eventsJson.put(assetUUID, assetMap);

        if (eventsJson.size() >= 25) {
            TreeMap<Integer,Map<String,Object>> sortedMap = new TreeMap<Integer,Map<String,Object>>();
            for (String key : eventsJson.keySet()) {
                Map<String,Object> eventMap = JSONUtil.getSafeMap(eventsJson, key);
                sortedMap.put(JSONUtil.getSafeInteger(eventMap, "timeOfVisit"), eventMap);
            }
            while (sortedMap.size() >= 25) {
                Integer eventKey = sortedMap.firstKey();
                sortedMap.remove(eventKey);
            }
        }
        JSONUtil.saveJSONData(this, "events", eventsJson);
    }

    private void showSecurityBanner(boolean ok, boolean hidden, boolean fade)
    {
        final View v = (ok ? mSecurityBackgroundGreen : mSecurityBackgroundRed);

        v.setAlpha(0f);
        v.setVisibility(View.VISIBLE);

        v.animate()
                .alpha(1f)
                .setDuration(1500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.animate()
                                .alpha(0f)
                                .setDuration(1500)
                                .setStartDelay(1500)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        v.setVisibility(View.GONE);
                                    }
                                });
                    }
                });
    }

    private void showSecurityElements(boolean isCounterfeit, boolean missingQR, boolean missingNFC, boolean nfcValid)
    {
        boolean valid = true;

        if (isCounterfeit) {
            valid = false;
        }
        /*
        else {
            if (nfcValid) {
                if (missingQR && iScanMode != kScanModeNFC) {
                    valid = false;
                }
            }
            if (missingNFC && iScanMode != kScanModeQR) {
                valid = false;
            }
        }
        */
        if (!missingNFC && !nfcValid) {
            valid = false;
        }
        showSecurityBanner(valid, false, true);
    }

    class ScanParams
    {
        String assetId, qrHash;
        Double latitude, longitude;

        public ScanParams(String assetId, String qrHash, Double latitude, Double longitude)
        {
            this.assetId = assetId;
            this.qrHash = qrHash;
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    private class ScanTask extends AsyncTask<ScanParams, Void, Map<String, Object>> {
        private ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(MainActivity.this, "", "Loading", true,
                    false); // Create and show Progress dialog
        }

        @Override
        protected Map<String, Object> doInBackground(ScanParams... params) {

            try {
                ScanParams scanParams = params[0];
                return AppServerClient.processScanFromApp(scanParams.assetId, scanParams.qrHash, scanParams.latitude, scanParams.longitude);

            } catch (Exception e) {
                Map<String, Object> errorMap = new HashMap<String, Object>();
                errorMap.put("error", e);
                return errorMap;
            }
        }

        @Override
        protected void onPostExecute(Map<String, Object> map) {
            pd.dismiss();
            try {
                processScanResults(map);
            } catch (Exception e) {}
        }
    }

    /*
    @Override
    public void onDialogDoneClick(int iAffinitySelect, int iScanModeSelect, String affinity)
    {
        Log.i("MainActivity", "Settings results: " + iAffinitySelect + " " + iScanModeSelect);
        iAffinity = iAffinitySelect;
        iScanMode = iScanModeSelect;
        this.affinity = affinity;
    }
    */

}
