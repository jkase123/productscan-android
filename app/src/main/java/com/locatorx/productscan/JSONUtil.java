package com.locatorx.productscan;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class JSONUtil
{
    public static String getSafeString (Map<String,Object> map, String key)
    {
        try {
            return (String)map.get(key);
        } catch (NullPointerException npe) {

        }
        return null;
    }

    public static String getSafeStringAlt (Map<String,Object> map, String key)
    {
        try {
            return (String)map.get(key);
        } catch (NullPointerException npe) {

        }
        return "";
    }

    @SuppressWarnings("unchecked")
    public static List<String> getSafeStringList (Map<String,Object> map, String key)
    {
        try {
            return (List<String>)map.get(key);
        } catch (NullPointerException npe) {

        }
        return new ArrayList<String>();
    }

    @SuppressWarnings("unchecked")
    public static List<UUID> getSafeUUIDList (Map<String,Object> map, String key)
    {
        List<UUID> results = new ArrayList<UUID>();
        try {
            List<Object> data = (List<Object>)map.get(key);
            for (Object o : data) {
                results.add(safeUUID(o));
            }
        } catch (NullPointerException npe) {

        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public static List<Integer> getSafeIntegerList (Map<String,Object> map, String key)
    {
        try {
            return (List<Integer>)map.get(key);
        } catch (NullPointerException npe) {

        }
        return new ArrayList<Integer>();
    }

    @SuppressWarnings("unchecked")
    public static List<Integer> getSafeIntegerListAlt (Map<String,Object> map, String key)
    {
        try {
            Object object = map.get(key);
            if (object instanceof List) {
                @SuppressWarnings("rawtypes")
                List objects = (List<Object>)object;
                if (objects.size() > 0) {
                    Object firstObj = objects.get(0);
                    if (firstObj instanceof Integer)
                        return (List<Integer>)objects;
                }
            }
        } catch (NullPointerException npe) {

        }
        return null;
    }

    public static Integer getSafeInteger(Map<String, Object> map, String key)
    {
        try {
            Object object = map.get(key);
            if (object instanceof Long) {
                Long longObject = (Long)object;
                return new Integer((int)longObject.longValue());
            } else if (object instanceof Double) {
                Double doubleObject = (Double) object;
                return new Integer((int)Math.round(doubleObject));
            } else if (object instanceof Float) {
                Float floatObject = (Float) object;
                return new Integer(Math.round(floatObject));
            } else if (object instanceof String) {
                return new Integer((String)object);
            }
            return (Integer) object;
        } catch (NullPointerException npe) {

        }
        return null;
    }

    public static Boolean getSafeBoolean(Map<String, Object> map, String key)
    {
        try {
            return (Boolean) map.get(key);
        } catch (NullPointerException e) {
        }
        return null;
    }

    public static boolean getSafeBooleanAlt(Map<String, Object> map, String key)
    {
        try {
            return (Boolean) map.get(key);
        } catch (NullPointerException e) {
        }
        return false;
    }

    public static Long getSafeLong(Map<String, Object> map, String key)
    {
        try {
            Object object = map.get(key);
            if (object instanceof Integer) {
                Integer integerObject = (Integer)object;
                return new Long(integerObject.intValue());
            } else if (object instanceof String) {
                return new Long((String)object);
            }
            return (Long) object;
        } catch (NullPointerException npe) {

        }
        return null;
    }

    public static byte[] getSafeByteBuffer(Map<String, Object> map, String key)
    {
        try {
            Object object = map.get(key);
            if (object instanceof ByteBuffer) {
                ByteBuffer buffer = (ByteBuffer)object;
                return buffer.array();
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static Date getSafeDate(Map<String, Object> map, String key)
    {
        try {
            Object object = map.get(key);
            if (object instanceof Long) {
                Long longObject = (Long)object;
                return new Date(longObject.longValue());
            } else if (object instanceof String) {
                String s = (String) map.get(key);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                return sdf.parse(s);
            }

        } catch (Exception e) {
        }
        return null;
    }

    /*
    public static LocalDateTime getSafeLocalDateTime(Map<String, Object> map, String key)
    {
        try {
            Object object = map.get(key);
            if (object instanceof String) {
                String s = (String) map.get(key);
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                return LocalDateTime.parse(s, dtf);
            }

        } catch (Exception e) {
        }
        return null;
    }
    */

    public static Timestamp getSafeTimestamp(Map<String, Object> map, String key)
    {
        try {
            Object object = map.get(key);
            if (object instanceof Long) {
                Long longObject = (Long)object;
                return new Timestamp(longObject.longValue());
            } else if (object instanceof String) {
                String s = (String) map.get(key);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                return new Timestamp(sdf.parse(s).getTime());
            }

        } catch (Exception e) {
        }
        return null;
    }

    public static UUID safeUUID(Object object)
    {
        if (object instanceof String)
            return UUID.fromString((String)object);
        if (object instanceof UUID)
            return (UUID)object;
        return null;
    }

    public static UUID getSafeUUID(Map<String, Object> map, String key)
    {
        try {
            return safeUUID(map.get(key));
        } catch (NullPointerException npe) {

        }
        return null;
    }

    public static Double getSafeDouble(Map<String, Object> map, String key)
    {
        try {
            Object object = map.get(key);
            if (object.getClass().equals(Double.class))
                return (Double) object;
            if (object.getClass().equals(Integer.class)) {
                Integer iValue = (Integer) object;
                return new Double((double) (iValue.intValue()));
            } else if (object instanceof String) {
                return new Double((String)object);
            }

        } catch (NullPointerException npe) {

        }
        return null;
    }

    public static Float getSafeFloat(Map<String, Object> map, String key)
    {
        try {
            Object object = map.get(key);
            if (object.getClass().equals(Float.class))
                return (Float) object;
            if (object.getClass().equals(Double.class)) {
                double dValue = (Double) object;
                return new Float((float)dValue);
            }
            if (object.getClass().equals(Integer.class)) {
                Integer iValue = (Integer) object;
                return new Float((float) (iValue.intValue()));
            } else if (object instanceof String) {
                return new Float((String)object);
            }

        } catch (NullPointerException npe) {

        }
        return null;
    }

    public static float getSafeFloatAlt(Map<String, Object> map, String key)
    {
        try {
            Object value = map.get(key);
            if (value.getClass().equals(Float.class))
                return (Float) value;
            if (value.getClass().equals(Double.class)) {
                double dValue = (Double) value;
                return new Float((float)dValue);
            }
            if (value.getClass().equals(Integer.class)) {
                Integer iValue = (Integer) value;
                return new Float((float) (iValue.intValue()));
            }
            if (value.getClass().equals(String.class)) {
                Float floatValue = new Float((String)value);
                return floatValue;
            }

        } catch (NullPointerException npe) {

        }
        return 0.0f;
    }

    @SuppressWarnings({ "rawtypes" })
    public static List getSafeList(Map<String, Object> map, String key)
    {
        try {
            return (List) map.get(key);
        } catch (NullPointerException npe) {

        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<Map<String,Object>> getSafeMapArray(Map<String,Object> map, String key)
    {
        try {
            return (List<Map<String,Object>>)map.get(key);
        } catch (NullPointerException npe) {

        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static List<Map<String,Object>> getSafeMapArrayAlt (Map<String,Object> map, String key)
    {
        try {
            Object object = map.get(key);
            if (object instanceof List) {
                @SuppressWarnings("rawtypes")
                List objects = (List<Object>)object;
                if (objects.size() > 0) {
                    Object firstObj = objects.get(0);
                    if (firstObj instanceof Map)
                        return (List<Map<String,Object>>)objects;
                }
            }
        } catch (NullPointerException npe) {

        }
        return null;
    }

    @SuppressWarnings({ "unchecked" })
    public static Map<String, Object> getSafeMap(Map<String, Object> map, String key)
    {
        try {
            return (Map<String, Object>) map.get(key);
        } catch (NullPointerException npe) {

        }
        return null;
    }

    @SuppressWarnings({ "unchecked" })
    public static Map<String, String> getSafeStringMap(Map<String, Object> map, String key)
    {
        try {
            return (Map<String, String>) map.get(key);
        } catch (NullPointerException npe) {

        }
        return null;
    }

    public static String jsonToString (Object outParams)
    {
        String outJSon = "";
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            outJSon = mapper.writeValueAsString(outParams);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return outJSon;
    }

    public static Map<String,Object> stringToJson(String jString)
    {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> inParams = mapper.readValue(jString, new TypeReference<HashMap<String, Object>>() {
            });

            return inParams;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new HashMap<String,Object>();
    }

    public static String getSafeMapToString(Map<String,Object> params, String key)
    {
        try {
            return jsonToString(getSafeMap(params, key));
        } catch (Exception e) {
        }
        return "";
    }

    public static Map<String, Object> stringToJsonMap(String jsonString)
    {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(jsonString, new TypeReference<HashMap<String, Object>>() { });
        } catch (Exception e) {
        }
        return new HashMap<String,Object>();
    }

    public static String jsonMapToString(Map<String, Object> outParams)
    {
        String outJSON = "";
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            outJSON = mapper.writeValueAsString(outParams);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return outJSON;
    }

    public static String jsonListToString(List<Object> outParams)
    {
        String outJSON = "";
        try {
            ObjectMapper mapper = new ObjectMapper();
            outJSON = mapper.writeValueAsString(outParams);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return outJSON;
    }

    public static String getDocumentFileString(Context context, String jsonTag)
    {
        return context.getExternalFilesDir(null) + "/json/"+jsonTag+".json";
    }

    public static File getDocumentFileDirectory(Context context)
    {
        return new File(context.getExternalFilesDir(null), "json/");
    }

    public static Map<String, Object> loadJSONData(Context context, String jsonTag)
    {
        Map<String,Object> result = new HashMap<String,Object>();
        try {
            File docDirectory = getDocumentFileDirectory(context);
            if (!docDirectory.exists())
                docDirectory.mkdirs();

            String jsonString = FileUtil.readFileToString(getDocumentFileString(context, jsonTag));
            return stringToJson(jsonString);

        } catch (Exception e) {
        }
        return result;
    }

    public static void saveJSONData(Context context, String jsonTag, Map<String,Object> json)
    {
        try {
            File docDirectory = getDocumentFileDirectory(context);
            if (!docDirectory.exists())
                docDirectory.mkdirs();

            String jsonString = jsonMapToString(json);
            FileUtil.writeStringToFile(getDocumentFileString(context, jsonTag), jsonString);

        } catch (Exception e) {
        }
    }

    public static Map<String, Object> fetchJson(String url, Map<String, Object> postBodyJson, Map<String,String> headerMap)
    {
        Map<String, Object> result = new HashMap<String, Object>();

        try {
            URL object = new URL(url);

            Log.i("JSONUtil","fetchJson url: " + url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("auth-token", "052df04a-b75f-4806-8a2b-0acdf2aa6a5d");

            if (headerMap != null) {
                for (String header : headerMap.keySet())
                    con.setRequestProperty(header, headerMap.get(header));
            }

            if (postBodyJson != null) {
                con.setDoOutput(true);
                con.setRequestProperty("Accept", "application/json");
                con.setRequestMethod("POST");

                OutputStream os = con.getOutputStream();
                String postBody = jsonMapToString(postBodyJson);
                Log.i("JSONUtil","postBody: " + postBody);
                os.write(postBody.getBytes("UTF-8"));
                os.close();
            }

            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                Log.i("JSONUtil",sb.toString());

                /*
                 * Map<String, List<String>> headerFields =
                 * con.getHeaderFields(); List<String> responseCookies =
                 * headerFields.get("Set-Cookie"); if (responseCookies != null)
                 * { result.put("cookies", responseCookies); }
                 */
                result.put("success", true);
                result.put("json", stringToJsonMap(sb.toString()));

            } else {
                Log.i("JSONUtil",con.getResponseMessage());
                result.put("error", "error code: " + Integer.toString(HttpResult));
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.put("error", e.getMessage());
        }
        return result;
    }
}
